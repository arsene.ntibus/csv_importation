<?php

include "cnx.php";
// En début de script je stock la date
$time_start = microtime(true);

// define créer et instentie une CONSTANTE avec comme premier paramètre le nom de la constante
// et comme deuxième parametre la valeur. Ici la constante prends le return de la fonction getCountry.

define("COUNTRIES", getCountry());
// Ici tout votre script à mesurer
// Je crée une variable qui contiendra le nom du fichier csv.
$csv = 'people.csv';
// J'ouvre le fichier en mode r,r correspond à read.
$csvfopen = fopen($csv, 'r');

if ($csvfopen !== false) {
    while (!feof($csvfopen)) {
        $line = fgetcsv($csvfopen);
        $email = $line[3];
        $id = $line[0];
        $first_name = $line[1];
        $last_name = $line[2];
        $email = (strtolower($line[3] . '<br>'));

      // Try catch car la prmière ligne du fichier CSV correspond au labèl des columns du fichier.
        try {
            $date_of_birth = DateTime::createFromFormat('d/m/Y', $line[5]);
            if (!is_bool($date_of_birth)) {
                $dateFormatYMD = $date_of_birth->format('Y-m-d');
            }
        } catch (Exception $e) {
//            echo 'Exception reçue : ', $e->getMessage(), "\n";
        };
        $country = $line[6];

        // J'assigne la valeur country grâce à la cle du tableau allCountry.
        $countCommonName = COUNTRIES[$country];
        $phone = $line[7];
        $email = strtolower($email);
        if (filter_var(strtolower($email), FILTER_VALIDATE_EMAIL)) {
            $email = strtolower($line[3]);
        } else {
            $email = "";
        }
        $job = $line[4];
        $newjob = '';
        $job == 'politicien' ? $newjob = 'menteur' : $newjob = $job;
        $phone = '0'.$phone;

        $db_connect = getDbConnect();
        $req = "INSERT INTO `people`(`id`, `firstname`, `lastname`, `email`, `profession`, `birthdate`, `country`, `phone`)
        VALUES ( :id, :first_name, :last_name,:email,:profession,:dateFormatYMD,:country,:phone)";
        $req = $db_connect->prepare($req);
        $params = ['id' => $id, 'first_name' => $first_name, 'last_name' => $last_name, 'email' => $email,
            'profession' => $newjob, 'dateFormatYMD' => $dateFormatYMD, 'country' => $countCommonName, 'phone' => (string)$phone];
        $req->execute($params);
        // reste à faire ,verifier si le fichier people-clean.csv existe ?? ajouter une nouvelle ligne : créer le fichier.
        $fp = fopen('people-clean.csv', 'w');
        $forCSV = [$id, $first_name, $last_name, $email, $newjob, $dateFormatYMD, $countCommonName, $phone];
        fputcsv($fp, $forCSV);
        fclose($fp);
        //
    }
}
function getCountry()
{
    $string = file_get_contents("names.json");
    return json_decode($string, true);
}